# !/usr/bin/python
# -*- coding: utf-8 -*-

import paho.mqtt.client as _client
import prepareMQTT as prepareMQTT

brokerAddr = "192.168.178.140"
brokerPort = 1883

prepare = prepareMQTT.prepareMQTT([brokerAddr, brokerPort])

client = _client.Client()
client.on_connect = prepare.on_connect
client.on_message = prepare.on_message

client.connect(brokerAddr, brokerPort)

client.loop_forever()
