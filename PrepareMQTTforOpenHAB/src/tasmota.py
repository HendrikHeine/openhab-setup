# !/usr/bin/python
# -*- coding: utf-8 -*-

import datetime
import time as _time
import json

class mt681:
    def __init__(self, value, timestamp) -> None:
        self.timestamp = timestamp
        jsonValue = json.loads(value)
        self.current = jsonValue['MT681']['Power_cur']
        self.total = jsonValue['MT681']['Total_in']

class smartMeter:
    def __init__(self, id:str, topic:str, name:str) -> None:
        self.id = id
        self.name = name
        self.topic = topic
        self.pricePerKWh = 0.44

        self.topic_tele_sensor = f"tele/{self.topic}/SENSOR"
        self.topic_curr = f"{self.topic}/{id}/CURRENT"
        self.topic_total = f"{self.topic}/{id}/TOTAL"
        self.topic_price = f"{self.topic}/{id}/PRICE"
        self.topic_priceForDay = f"{self.topic}/{id}/PRICE_FOR_DAY"
        self.topic_kwhForDay = f"{self.topic}/{self.id}/KWH"

    def getTime(self):
        dateNow = datetime.datetime.now()
        dateNow = str(dateNow).split(" ")
        unix = _time.time()
        time = dateNow[1]
        time = time.split(".")
        time = time[0]
        dateNow = dateNow[0]
        # Date, time, unix timestamp
        value = [ dateNow, time, unix ]
        return value

    def getMessure(self, value):
        return mt681(value, self.getTime()[2])