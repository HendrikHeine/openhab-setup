# !/usr/bin/python
# -*- coding: utf-8 -*-

import paho.mqtt.client as _client
from paho.mqtt.client import MQTTMessage
import tasmota as tasmota
import os
import logging

logger = logging.getLogger('app')
logger.setLevel(logging.DEBUG)
handler = logging.FileHandler(filename='data/mqtt.log', encoding='utf-8', mode='a')
handler.setFormatter(logging.Formatter('%(asctime)s:%(levelname)s:%(name)s: %(message)s'))
logger.addHandler(handler)


class prepareMQTT:
    def __init__(self, addr) -> None:
        self.brokerAddr = addr[0]
        self.brokerPort = addr[1]
        self.messure:tasmota.mt681 = ""
        self.smartMeter = tasmota.smartMeter(id="2DA2EB", topic="smartmeter", name="SmartMeter")
        self.date = ""
        self.value = self.readValue()

    def on_message(self, client:_client, userdata, message:MQTTMessage):
        msg = message.payload.decode("utf-8")

        if message.topic == self.smartMeter.topic_tele_sensor:
            self.messure = self.smartMeter.getMessure(msg)
            
            if self.date == "":
                self.date = self.smartMeter.getTime()[0]

            if self.value == "":
                self.writeValue(str(self.messure.total))
                self.value = self.readValue()
                print(self.value)
                logger.info(self.value)

            if self.date != self.smartMeter.getTime()[0]:
                logger.info("New Day")
                self.value = self.readValue()
                self.date = self.smartMeter.getTime()[0]
                self.writeValue(str(self.messure.total))
                price = float((self.messure.total - float(self.value)) * self.smartMeter.pricePerKWh)
                client.publish(topic=self.smartMeter.topic_priceForDay, payload=str(price))
            
            value = float(self.readValue())
            price = float((self.messure.total - value) * self.smartMeter.pricePerKWh)

            client.publish(topic=self.smartMeter.topic_price, payload=str(price))
            client.publish(topic=self.smartMeter.topic_kwhForDay, payload=str(self.messure.total - value))
            client.publish(topic=self.smartMeter.topic_curr, payload=self.messure.current)
            client.publish(topic=self.smartMeter.topic_total, payload=self.messure.total)


    def on_connect(self, client:_client.Client, userdata, flags, rc):
        print(f"Connected to MQTT Broker: {self.brokerAddr}:{self.brokerPort}")
        client.subscribe(self.smartMeter.topic_tele_sensor)

    def writeValue(self, value):
        try:os.remove("data/value.txt")
        except:pass
        with open("data/value.txt", "w") as file:
            file.write(value)
            file.close()

    def readValue(self):
        if os.path.exists("data/value.txt"):
            with open("data/value.txt", "r") as file:
                value = file.readlines()
                file.close()
                if value == []:
                    return ""
                else:
                    return value[0]
        else:
            return ""
