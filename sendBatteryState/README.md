# config.json
Create the json file in the root directory
```json
{
    "baseURL": "https://myopenhab.org/rest",
    "username": "user@email.de",
    "password": "YourPassword",
    "apiToken": "",
    "deviceName": "DeviceName",
    "sleepTimeInSec": 10,
    "post_Temperature": true,
    "post_Battery": true,
    "post_RAM": true,
    "post_CPU": true  
}
```