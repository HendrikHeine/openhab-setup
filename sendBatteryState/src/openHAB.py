from openhab import OpenHAB
from openhab.items import Item

from models.models import Battery


class OpenHab:
    def __init__(self, baseURL:str, deviceName:str, username:None, password:None, apiToken:None) -> None:
        self.__base_url = baseURL
        self.__deviceName = deviceName

        if apiToken == "" and username != "" and password != "": # username and password login
            print("User/Pass Login\n")
            self.__openHab = OpenHAB(base_url=self.__base_url, username=username, password=password)

        elif apiToken != "": # api token login
            print("API Token Login\n")
            self.__openHab = OpenHAB(base_url=baseURL, username=username)

        else: # no login
            print("No Login\n")
            self.__openHab = OpenHAB(base_url=baseURL)

    def getAllItems(self):
        print(self.__openHab.fetch_all_items())

    def postBattery(self, battery:Battery):
        item = self.__openHab.get_item(f"BatteryLevel_{self.__deviceName}")
        item.update(battery.percent)

        item = self.__openHab.get_item(f"ChargingState_{self.__deviceName}")
        item.update(str(battery.powerPlugged))

    def postTemperature(self, temperature:float):
        item = self.__openHab.get_item(f"Temperature_{self.__deviceName}")
        item.update(temperature)

    def postRAM(self, ram):
        item = self.__openHab.get_item(f"RAM_{self.__deviceName}")
        item.update(ram)

    def postCPU(self, cpu):
        item = self.__openHab.get_item(f"CPU_{self.__deviceName}")
        item.update(cpu)