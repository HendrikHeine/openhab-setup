import psutil
import statistics
import json
import time

from models.models import Battery
from models.models import Temperature
from models.models import Config
from openHAB import OpenHab

try:
    with open(file="config.json", mode="r") as file:
        config = Config(json.loads(file.read()))
        file.close()
except:
    print("No config file found or config file is damaged")
    exit(1)

openHAB = OpenHab(
    baseURL=config.baseURL, 
    username=config.username, 
    password=config.password, 
    deviceName=config.deviceName,
    apiToken=config.apiToken
)

def postTemp():
    temp = psutil.sensors_temperatures()
    temps = []
    for tempInTemperature in temp:
        for i in temp[tempInTemperature]:
            temperature = Temperature(i)
            if temperature.label != '':
                temps.append(temperature.current)
    
    print(f"Temp: {statistics.median(temps)}°c")
    openHAB.postTemperature(temperature=float(statistics.median(temps)))

def postBattery():
    battery = Battery(psutil.sensors_battery())
    print(f"Battery: {round(battery.percent, 1)}%")
    print(f"Battery Plugged: {battery.powerPlugged}")
    openHAB.postBattery(battery=battery)

def postRAM():
    ram = psutil.virtual_memory()
    print(f"RAM: {ram.percent}%")
    openHAB.postRAM(ram=ram.percent)

def postCPU():
    cpuPercent = psutil.cpu_percent()
    print(f"CPU {cpuPercent}%")
    openHAB.postCPU(cpu=cpuPercent)


while True:
    if config.post_Battery: postBattery()
    if config.post_Temperature: postTemp()
    if config.post_RAM: postRAM()
    if config.post_CPU: postCPU()

    print(f"\nSleep {config.sleep}sec\n")
    time.sleep(config.sleep)
