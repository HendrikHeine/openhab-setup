class Battery:
    def __init__(self, data) -> None:
        self.percent:int = int(data.percent)
        self.secondsLeft:str = data.secsleft
        self.powerPlugged:bool = data.power_plugged

class Temperature:
    def __init__(self, data) -> None:
        self.label:str = data.label
        self.current:float = data.current
        self.high:float = data.high
        self.critical:float = data.critical

class Config:
    def __init__(self, data) -> None:
        self.baseURL:str = data['baseURL']
        self.password:str = data['password']
        self.username:str = data['username']
        self.deviceName:str = data['deviceName']
        self.apiToken:str = data['apiToken']
        self.sleep:int = data['sleepTimeInSec']
        self.post_Temperature:bool = data['post_Temperature']
        self.post_Battery:bool = data['post_Battery']
        self.post_RAM:bool = data['post_RAM']
        self.post_CPU:bool = data['post_CPU']
        