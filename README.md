# OpenHAB Setup

Hier sind alle Config Dateien und Programme die ich für OpenHAB nutze

## Services

In einigen Ordnern sind .service Dateien. Die müssen modifiziert werden. In der Zeile `ExecStart=` muss der Pfad zum Programm eingetragen werden. Dann die `.service` Datei in den Order /etc/systemd/system/ verschieben. Dann die daemon Jobs mit `sudo systemctl daemon-reload` neuladen, mit `sudo systemctl enable nameVomDienst.service` den Service aktivieren und mit `sudo systemctl start nameVomDienst.service` starten. Der Status lässt sich mit `sudo systemctl status nameVomService.service` sehen. Zum Anhalten vom Service `sudo systemctl stop nameVomService.service` eingeben.

## Software

[MQTT Client für Desktop](https://mqttx.app/), </br>
[VScode Exstension](https://www.openhab.org/docs/configuration/editors.html#openhab-vs-code-extension), </br>
[Terninator](https://terminator-gtk3.readthedocs.io/en/latest/), </br>

## Links

[OpenHAB](https://www.openhab.org), </br>
[OpenHAB Doc](https://www.openhab.org/docs/), </br>
[OpenHAB Addons](https://www.openhab.org/addons/), </br>
[SmartMeter](https://hessburg.de/tasmota-wifi-smartmeter-konfigurieren/),</br>
[Tasmota](https://tasmota.github.io/docs/),</br>
