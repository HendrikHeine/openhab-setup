from gpiozero import LED

class mqttRelais:
    def __init__(self, id:str, pin:int, on_start_on=False) -> None:
        self.pin = pin
        self.id = id
        self.state = ""
        self.ON = "ON"
        self.OFF = "OFF"
        self.relais = ""
        self.topic = f"rpi/relais/{self.id}"
        self.topic_set = f"{self.topic}/SET"
        self.topic_status = f"{self.topic}/STATUS"
        if on_start_on:
            self.turnOn()
        else:
            self.turnOff()

    def turnOn(self):
        self.relais = LED(pin=self.pin)
        self.relais.on()
        self.state = self.ON

    def turnOff(self):
        try:self.relais.close()
        except:pass
        self.state = self.OFF

