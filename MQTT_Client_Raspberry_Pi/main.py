import thing
import paho.mqtt.client as _client
from paho.mqtt.client import MQTTMessage

brokerAddr = "192.168.178.51"
brokerPort = 1883

class prepareMQTT:
    def __init__(self) -> None:
        self.relais_01 = thing.mqttRelais("REGAL_001", 17)
        self.relais_02 = thing.mqttRelais("REGAL_002", 27)

    def on_message(self, client:_client, userdata, message:MQTTMessage):
        msg = message.payload.decode("utf-8")

        if message.topic == self.relais_01.topic_set:
            if msg == self.relais_01.ON:
                self.relais_01.turnOn()
                client.publish(topic=self.relais_01.topic_status, payload=self.relais_01.state)
            if msg == self.relais_01.OFF:
                self.relais_01.turnOff()
                client.publish(topic=self.relais_01.topic_status, payload=self.relais_01.state)

        if message.topic == self.relais_01.topic_status:
            client.publish(topic=self.relais_01.topic_status, payload=self.relais_01.state)

        if message.topic == self.relais_02.topic_set:
            if msg == self.relais_02.ON:
                self.relais_02.turnOn()
                client.publish(topic=self.relais_02.topic_status, payload=self.relais_02.state)

            if msg == self.relais_02.OFF:
                self.relais_02.turnOff()
                client.publish(topic=self.relais_02.topic_status, payload=self.relais_02.state)

        if message.topic == self.relais_02.topic_status:
            client.publish(topic=self.relais_02.topic_status, payload=self.relais_02.state)

    def on_connect(self, client:_client.Client, userdata, flags, rc):
        print("Connected to MQTT Broker: " + brokerAddr)
        client.subscribe(self.relais_01.topic_set)
        client.subscribe(self.relais_02.topic_set)

prepare = prepareMQTT()
client = _client.Client()
client.on_connect = prepare.on_connect
client.on_message = prepare.on_message

client.connect(brokerAddr, brokerPort)

client.loop_forever()
